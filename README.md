## Harpoon

[![build status](https://gitlab.com/snowypowers/harpoon/badges/master/build.svg)](https://gitlab.com/snowypowers/harpoon/commits/master)

A fork of Agrison's Harpoon, this version is specific to GitLab hooks. 

### Install

```sh
go get gitlab.com/snowypowers/harpoon
go build
```

Add it to your path if you want.

### Runnning

#### Configuration

You must have a `config.toml` file located in your directory.

This is a sample TOML file:

```toml
port = 9001
addr = "0.0.0.0"
# tunneling stuff
tunnel = true
tunnelName = "foobarbazz"

[events."push:foo/bar"]
cmd = "echo"
args = "Push!"

[events."tag_push:foo/bar"]
cmd = "echo"
args = "Pushed Tag!"
```

As you can see events and refs can be configured in the `events` TOML table section.

These keys have the following format `events.{event}:{repository}` where `{event}` refers to a GitLab WebHook event
like `push`, `watch`, `pull_request`, ...; `{repository}` is the GitLab repository name.

For the above example, it will echo `Push!` when something has been pushed to the `develop` branch of your GitLab repository. 

#### Tunneling

Tunneling can be enabled using `localtunnel.me`, you just have to enable it in the `config.toml` and indicate a desired subdomain using the `tunnelName` field.
If no `tunnelName` is provided then `localtunnel.me` will give you a random subdomain, this subdomain is printed in the console logs.

#### Really running

```sh
harpoon
```

or to have it verbose (use `-vt` also if you want logs about `localtunnel.me`):
```sh
harpoon -v
```

It will output something like this when running:
```
    __
   / /_  ____ __________  ____  ____  ____
  / __ \/ __ `/ ___/ __ \/ __ \/ __ \/ __ \
 / / / / /_/ / /  / /_/ / /_/ / /_/ / / / /
/_/ /_/\__,_/_/  / .___/\____/\____/_/ /_/
                /_/
	Listening on 0.0.0.0:9001
    
push detected on foo/bar with ref refs/heads/develop with the following commits:
	2016-03-08 13:59:38 +0100 CET - adding some awesomeness by Foo Bar
	2016-03-08 13:59:57 +0100 CET - forgot the unicorn png by Foo Bar
	2016-03-08 14:01:12 +0100 CET - so much joy in one commit by Foo Bar
> Push!
```


Obviously, the command should be something like `/path/to/pull-build-n-deploy.sh` where some awesome stuff is going on. 
Like something involving git, maven, rake, npm, bower, I don't know, this is your job :)

### Security

`POST` requests made by GitLab are validated against the `X-Gitlab-Token` if the environment variable `GITLAB_HOOK_SECRET_TOKEN` is set.
It must be the same as the one defined on the GitLab's WebHook page.

### References

[Original Repo](https://github.com/agrison/harpoon)

### Licence
MIT
