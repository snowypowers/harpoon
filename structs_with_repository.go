package main

// HookWithRepository represents an event message sent by Github that contains a "repository" field.
type eventProject struct {
	ObjectKind string `json:"object_kind"`
	Project struct {
		Name string `json:"name"`
		Description string `json:"description"`
		WebURL string `json:"web_url"`
		AvatarURL interface{} `json:"avatar_url"`
		GitSSHURL string `json:"git_ssh_url"`
		GitHTTPURL string `json:"git_http_url"`
		Namespace string `json:"namespace"`
		VisibilityLevel int `json:"visibility_level"`
		PathWithNamespace string `json:"path_with_namespace"`
		DefaultBranch string `json:"default_branch"`
		Homepage string `json:"homepage"`
		URL string `json:"url"`
		SSHURL string `json:"ssh_url"`
		HTTPURL string `json:"http_url"`
	} `json:"project"`
}
